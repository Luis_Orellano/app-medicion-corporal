import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GeneralParametersPageRoutingModule } from './general-parameters-routing.module';

import { GeneralParametersPage } from './general-parameters.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeneralParametersPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [GeneralParametersPage]
})
export class GeneralParametersPageModule {}
