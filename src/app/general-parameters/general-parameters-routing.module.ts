import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeneralParametersPage } from './general-parameters.page';

const routes: Routes = [
  {
    path: '',
    component: GeneralParametersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneralParametersPageRoutingModule {}
