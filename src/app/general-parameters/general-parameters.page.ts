import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from "@ionic/storage-angular"
import { SendLoteDataService } from '../send-lote-data.service';

@Component({
  selector: 'app-general-parameters',
  templateUrl: './general-parameters.page.html',
  styleUrls: ['./general-parameters.page.scss'],
})
export class GeneralParametersPage implements OnInit {
  datosLote: any;
  escala = '';
  etapa = '';

  historicos;

  storage: Storage;

  constructor(private dataService: SendLoteDataService, private _storage: Storage, private router: Router) { }

  async ngOnInit() {
    this.dataService.$getObjectSource.subscribe((data) => {
      this.datosLote = data;
      console.log('GeneralParametersPage: ' ,this.datosLote);
    });
    this.storage = await this._storage.create();
  }

  async onSubmit() {
    let now = new Date();

    let newLote = {
      campo: this.datosLote.name,
      rodeo: this.datosLote.rodeo,
      idRodeo: this.datosLote.idRodeo,
      ubicacion: this.datosLote.location,
      registro: this.datosLote.registro,
      escala: { 
        id: this.escala == '1-5' && this.escala.length > 0 ? 0 : 1,
        descrip: this.escala
      },
      etapa: { 
        id: this.etapa == 'Parto' && this.etapa.length > 0 ? 0 : this.etapa == 'Servicio' && this.etapa.length > 0 ? 1 : 2,
        descrip: this.etapa
      },
      fechaMedicion: now.toISOString(),
      valorPromedio: null,
      resultado: null
    }

    console.log(newLote);

    let arrayHistoricos = JSON.parse(await this.storage.get('historico')) || [];
    arrayHistoricos.unshift(newLote);
    let arrayHistoricosJSON = JSON.stringify(arrayHistoricos);
    this.storage.set('historico', arrayHistoricosJSON);

    this.dataService.sendObject(newLote);

    this.router.navigate(['/medicion']);
  }

}