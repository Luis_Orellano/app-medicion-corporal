import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'identifi-lote',
    loadChildren: () => import('./identifi-lote/identifi-lote.module').then( m => m.IdentifiLotePageModule)
  },
  {
    path: 'general-parameters',
    loadChildren: () => import('./general-parameters/general-parameters.module').then( m => m.GeneralParametersPageModule)
  },
  {
    path: 'medicion',
    loadChildren: () => import('./medicion/medicion.module').then( m => m.MedicionPageModule)
  },
  {
    path: 'modal-condicion',
    loadChildren: () => import('./modal-condicion/modal-condicion.module').then( m => m.ModalCondicionPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
