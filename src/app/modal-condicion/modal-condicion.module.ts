import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalCondicionPageRoutingModule } from './modal-condicion-routing.module';

import { ModalCondicionPage } from './modal-condicion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalCondicionPageRoutingModule
  ],
  declarations: [ModalCondicionPage]
})
export class ModalCondicionPageModule {}
