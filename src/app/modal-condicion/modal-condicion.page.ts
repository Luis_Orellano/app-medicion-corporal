import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-condicion',
  templateUrl: './modal-condicion.page.html',
  styleUrls: ['./modal-condicion.page.scss'],
})
export class ModalCondicionPage implements OnInit {
  @Input() lote: any;
  
  condicionCritica = {
    partoServicio: {
      unoCinco: 3,
      unoNueve: 6
    },
    destete : {
      unoCinco: 3.5,
      unoNueve: 5
    }
  }

  recomendaciones: any;



  constructor(public modalController: ModalController) { }

  ngOnInit() {
    console.log(this.lote);    
    this.recomendaciones = {
      'parto': {
        'msjComunMenorIgualTresMenorIgualSieteMayorIgualSeisMenorSeisMayorTres': "Para mejorar la preñez en el próximo servicio, puede contemplar adelantar el destete de las vacas con condición corporal menor a ",
        'msjComunMenorIgualTresMenorIgualSieteMayorIgualSeisMenorSeisMayorTres2':" y/o mejorar el plano nutricional de los animales de menor condición",
        'mayorSiete': {
          'msjComun': "El exceso de condición corporal al parto puede dificultar la parición por exceso de grasa en el canal de parto. Deberá tener cuidado con la alimentación posparto ya que un descenso brusco de condición de animales muy gordos, puede generar problemas metabólicos como la cetosis"
         }
      },
      'servicio': {
        'menorSeisMayorTres': {
          'msjComunUnoCincoUnoNueve': "Para mejorar la preñez en el próximo servicio, puede contemplar adelantar el destete de las vacas con condición corporal menor a ",
          'msjComunUnoCincoUnoNueve2': " y/o mejorar el plano nutricional de los animales de menor condición",
        },
        'menorIgualSieteMayorIgualSeis': {
          'msjComun': "En lo posible, procure mantener la condición durante el servicio en un plano ligeramente ascendente para asegurar la máxima tasa de preñez"
        },
        'menorIgualTres': {
          'msjComunUnoCincoUnoNueve': "En esta situación sería recomendable verificar la ciclisidad de cada vaca para tomar decisiones en consecuencia. Para mejorar la preñez durante el servicio, puede contemplar adelantar el destete de las vacas con condición corporal menor a ",
          'msjComunUnoCincoUnoNueve2' :" y/o mejorar el plano nutricional de los animales de menor condición siempre que estén ciclando",
        },
        'mayorSiete': {
          'msjComunUnoCincoUnoNueve': "La condición corporal supera el nivel crítico para un óptimo desempeño reproductivo, puede contemplar alargar la lactancia de las vacas con condición corporal superior a ",
          'msjComunUnoCincoUnoNueve2': ", siempre cuidando que no descienda demasiado la condición"
         }
      },
      'destete': {
        'menorSeisMayorTres': {
          'unoCinco': {
            'msj1': "La condición corporal crítica al DESTETE es 3.5",
          },
          'unoNueve': {
            'msj1': "La condición corporal crítica al DESTETE es 6",
          },
          'msjComun': "Procure mejorar la condición corporal de sus vacas. Para mejorar la preñez en el próximo servicio, deberá mejorar el plano nutricional de los animales de baja condición. Sería aconsejable registrar el estado de ciclisidad de sus vacas para tomar medidas de manejo en consecuencia"
        },
        'menorIgualTres': {
          'unoCinco': {
            'msj1': "La condición corporal crítica al DESTETE es 3.5",
          },
          'unoNueve': {
            'msj1': "La condición corporal crítica al DESTETE es 6",
          },
          'msjComun': "Procure mejorar la condición corporal de sus vacas. Para mejorar la preñez en el próximo servicio, deberá mejorar el plano nutricional de los animales de baja condición. Sería aconsejable registrar el estado de ciclisidad de sus vacas para tomar medidas de manejo en consecuencia",
        },
        'menorIgualSieteMayorIgualSeis': {
          'unoCinco': {
            'msj1': "La condición corporal crítica al DESTETE es 3.5"
          },
          'unoNueve': {
            'msj1': "La condición corporal crítica al DESTETE es 6"
          },
          'msjComun2': "Procure mantener la condición corporal de sus vacas"
        },
        'mayorSiete': {
          'unoCinco': {
            'msj1': "La condición corporal crítica al DESTETE es 3.5",
          },
          'unoNueve': {
            'msj1': "La condición corporal crítica al DESTETE es 6",
          },
         },
         'msjComunMenorIgualSieteMayorIgualSeisMayorSiete': "Procure mantener la condición corporal de sus vacas",
      },
      'msjComunCondicion': "La condición corporal crítica al " + this.lote.etapa.descrip.toString().toUpperCase() + " es ",
      'msjComunPrenez': this.getCondicionCorporal(),
      'msjComunCC': "El " + this.lote.porcentajeBajoCC.toFixed(0) + "% de su rodeo está por debajo de la condición crítica",
    }    
  }

  getCondicionCorporal() {
    if (this.lote.escala.descrip == '1-5') {
      if (this.lote.promedioTotalUnoCinco <= 1.5) {
        return this.momentoLote();
      } else {
        return "Con la condición registrada, el potencial de preñez de su rodeo, rondará el " + this.lote.resultado + "%";
      }
    } else {
      if (this.lote.valorPromedio <= 2) {
        return this.momentoLote();
      } else {
        return "Con la condición registrada, el potencial de preñez de su rodeo, rondará el " + this.lote.resultado + "%";
      }
    }    
  }

  momentoLote() {
    if (this.lote.etapa.descrip == 'Destete') {
      return 'Su rodeo de vacas está en riesgo de vida por bajo peso, mejore la alimentación las vacas en baja condición'
    } else if (this.lote.etapa.descrip == 'Servicio') {
      return 'Su rodeo de vacas está en riesgo de vida por bajo peso, mejore la alimentación de las vacas en baja condición y destete cuanto antes si aún no lo hizo'
    } else if (this.lote.etapa.descrip == 'Parto') {
      return 'Su rodeo de vacas está en riesgo de vida por bajo peso, mejore la alimentación de las vacas en baja condición y destete lo antes posible'
    }
  }

  getDescripcionEscala() {
    if (this.lote.escala.descrip == '1-5') {
      if (this.lote.etapa.descrip == 'Destete') {
        return '3.5';
      } else {
        return '3';
      }
    } else {
      if (this.lote.etapa.descrip == 'Destete') {
        return '6';
      } else {
        return '5';
      }
    }
  }

  closeModal() {
    this.modalController.dismiss();
  }

}
