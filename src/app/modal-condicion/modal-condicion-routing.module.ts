import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalCondicionPage } from './modal-condicion.page';

const routes: Routes = [
  {
    path: '',
    component: ModalCondicionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalCondicionPageRoutingModule {}
