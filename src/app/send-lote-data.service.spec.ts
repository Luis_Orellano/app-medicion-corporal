import { TestBed } from '@angular/core/testing';

import { SendLoteDataService } from './send-lote-data.service';

describe('SendLoteDataService', () => {
  let service: SendLoteDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SendLoteDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
