import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage-angular"

@Injectable({
  providedIn: 'root'
})
export class StorageServiceService {
  private storage: Storage;

  constructor(private _storage: Storage) { 
    this.initStorage();
  }

  // setItem(key, value) {
  //   this.nativeStorage.setItem(key, value);
  // }

  // getItem(key) {
  //   return this.nativeStorage.getItem(key);
  // }

  async initStorage() {
    console.log('iniciando');
    
    const storage = await this._storage.create();
    this.storage = storage;
  }

  setData(key, value) {
    this.storage?.set(key, value);
  }

  getData(key) {
    this.storage?.get(key);
  }

  clearData() {
    this.storage?.clear();
  }

  deleteData(key) {
    this.storage?.remove(key);
  }

  setObject(key, data): any {
    return this.storage?.set(key, JSON.stringify(data));
  }
}


