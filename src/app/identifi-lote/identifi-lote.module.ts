import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IdentifiLotePageRoutingModule } from './identifi-lote-routing.module';

import { IdentifiLotePage } from './identifi-lote.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IdentifiLotePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [IdentifiLotePage]
})
export class IdentifiLotePageModule {}
