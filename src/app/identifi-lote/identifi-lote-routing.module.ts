import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IdentifiLotePage } from './identifi-lote.page';

const routes: Routes = [
  {
    path: '',
    component: IdentifiLotePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IdentifiLotePageRoutingModule {}
