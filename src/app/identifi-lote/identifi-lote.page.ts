import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SendLoteDataService } from '../send-lote-data.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-identifi-lote',
  templateUrl: './identifi-lote.page.html',
  styleUrls: ['./identifi-lote.page.scss'],
})
export class IdentifiLotePage implements OnInit {

  private formData: FormGroup;
  result: any;
  nameCampos: string[];
  rodeoOptions: string[];
  locations = [];

  constructor(private router: Router, private dataService: SendLoteDataService, private alertController: AlertController) { }

  ngOnInit() {
    this.initForm();
    this.dataService.getLocationAndRodeo()
      .then((data) => {
        this.result = data;
        console.log('getLocationAndRodeo: ', data);
        this.nameCampos = this.result.data.map(item => item.name);
        console.log('nameCampos: ',this.nameCampos);

      })
      .catch((error) => {
        console.error('getLocationAndRodeo: ', error);
      })
  }

  initForm() {
    this.formData = new FormGroup({
      name: new FormControl(),
      rodeo: new FormControl(),
      idRodeo: new FormControl(),
      location: new FormControl()
    })
  }

  goGeneralParameters() {
    const selectedRodeos = this.result.data.filter(item => item.name === this.formData.get('name').value)[0].rodeos;
    const rodeoSelected = selectedRodeos.filter(rodeo => rodeo.desc === this.formData.get('rodeo').value);
    this.formData.get('idRodeo').setValue(rodeoSelected[0].id);

    let data = {
      name: this.formData.get('name').value,
      rodeo: this.formData.get('rodeo').value,
      idRodeo: this.formData.get('idRodeo').value,
      location: this.formData.get('location').value,
      registro: 0
    }

    this.dataService.sendObject(data);
    this.router.navigate(['/general-parameters']);
  }

  async onChangeCampo(event: any) {
    console.log('this.result: ', this.result);
    const selectedCampo = event.target.value;
    const selectedRodeos = this.result.data.filter(item => item.name === selectedCampo)[0].rodeos;
    this.rodeoOptions = selectedRodeos.map(rodeo => rodeo.desc);
    this.formData.get('rodeo').setValue(selectedRodeos[0]);
    const selectData = this.result.data.find(item => item.name === selectedCampo);
    this.locations = [];
    this.formData.get('location').setValue(null);
    this.locations.push(selectData.location);
  }

}
