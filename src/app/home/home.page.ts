import { Component, OnInit } from '@angular/core';
import { Storage } from "@ionic/storage-angular"
import { ModalController } from '@ionic/angular';
import { ModalCondicionPage } from "../modal-condicion/modal-condicion.page";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  historico: Historicos[];
  cant = 0;
  resultado = 0;

  storage: Storage;

  constructor(private _storage: Storage, private modalCtrl: ModalController) {}

  ionViewWillEnter() {
    console.log('Cargando nuevos lotes...')
    this.getLotes();
  }

  async ngOnInit() {
    this.storage = await this._storage.create();
    this.getLotes();
  }

  getLotes() {
    this.historico = [];
    this.storage.get('historico').then((data) => {
      this.storage.length().then((l) => {
        let length = l;
        if (length > 0) {
          let dataLocal = JSON.parse(data);
          dataLocal.forEach(element => {
            this.historico.push(element);  
          });
          this.cant = this.historico.length;
          console.log(this.historico);
        }
      });
    });
  }

  doRefresh(event?) {
    console.log(event);
    
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getLotes();
      event.target.complete();
    }, 2000);
  }

  async openModal(item) {
    const modal = await this.modalCtrl.create({
      component: ModalCondicionPage,
      cssClass: 'my-custom-class',
      componentProps: {
        'lote': item
      }
    });
    return await modal.present();
  }

  getDescripcionEscala(item) {
    if (item.escala.descrip == '1-5') {
      if (item.etapa.descrip == 'Destete') {
        return '3.5';
      } else {
        return '3';
      }
    } else {
      if (item.etapa.descrip == 'Destete') {
        return '6';
      } else {
        return '5';
      }
    }
  }

  isRodeoRiesgo(item) {
    if (item.escala.descrip == '1-5') {
      if (item.promedioTotalUnoCinco <= 1.5) {
        return 'Alerta, rodeo en riesgo de vida';
      } else {
        return 'Preñez potencial: ' + this.menorOMayorA(item) + item.resultado + '%';
      }
    } else {
      if (item.valorPromedio <= 2) {
        return 'Alerta, rodeo en riesgo de vida';
      } else {
        return 'Preñez potencial: ' + this.menorOMayorA(item) + item.resultado + '%';
      }
    }    
  }

  menorOMayorA(item) {
    if (item.etapa.descrip == 'Destete') {
      if (item.resultado <= 76) {
        return 'Menor a ';
      } else {
        return 'Mayor a ';
      }
    } else if (item.etapa.descrip == 'Parto') {
      if (item.resultado <= 48) {
        return 'Menor a ';
      } else {
        return 'Mayor a ';
      }
    } else if (item.etapa.descrip == 'Servicio') {
      if (item.resultado <= 50) {
        return 'Menor a ';
      } else {
        return 'Mayor a ';
      }
    }
  }

}

interface Historicos {
  campo: string,
  rodeo: string,
  ubicacion: string,
  escala?: { id: number, descrip: string},
  etapa?: { id: number, descrip: string},
  registros?: number,
  valorpromedio?: number,
  resultado?: string,
  promedioTotalUnoCinco?: number;
  fechamedicion?: string,
  desviacionStandar: number,
  coeficiente: number,
  porcentajeBajoCC?: number,
}