import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../environments/environment';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class SendLoteDataService {
  private objectSource = new BehaviorSubject<{}>({});

  $getObjectSource = this.objectSource.asObservable();

  dataToSend: {
    "userId":number,
    "sampling":[
        {
         "rodeoId":number,
         "location":{
            "lat":number,
            "lng":number
         },
         "moment":string,
         "scaleMax":number,
         "date":string,
         "ccAvg":number
        }
    ]
  };

  constructor(private http: HttpClient, private geolocation: Geolocation) { }

  sendObject(data: any) {
    this.objectSource.next(data);
  }

  getLocationAndRodeo(idUser = 1): Promise<any> {
    return this.http.get(environment.apiUrl+`/rodeo/all/${idUser}`).toPromise();
  }

  async postData(dataLote: any) {
    console.log('PostData: ', dataLote);
    await this.prepareDataToSend(dataLote);
    return this.http.post(environment.apiUrl+'/sampling/new', this.dataToSend);
  }

  async prepareDataToSend(dataLote: any) {
    let position: {lat: number, lng: number};
    await this.geolocation.getCurrentPosition().then((resp) => {
      console.log('getCurrentPosition: ', resp);
      position = {
        lat: resp.coords.latitude,
        lng: resp.coords.longitude
      };
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    this.dataToSend = {
      userId: 1,
      sampling: [
        {
          rodeoId: dataLote.idRodeo,
          location: {
            lat: position.lat,
            lng: position.lng
          },
          moment: dataLote.etapa.descrip,
          scaleMax: dataLote.escala.descrip === '1-9' ? 9 : 5,
          date: dataLote.fechaMedicion,
          ccAvg: dataLote.valorPromedio
        }
      ]
    }
  }
}
