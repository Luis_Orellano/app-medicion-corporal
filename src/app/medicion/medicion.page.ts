import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Storage } from "@ionic/storage-angular"
import { SendLoteDataService } from '../send-lote-data.service';

@Component({
  selector: 'app-medicion',
  templateUrl: './medicion.page.html',
  styleUrls: ['./medicion.page.scss'],
})
export class MedicionPage implements OnInit {
  datosLote: any;
  registro = 0;
  escala = '';
  campo = '';
  rodeo = '';
  etapa = '';
  ubicacion = '';
  valorTotal = 0;
  valorTotalUnoCinco = 0;
  cantBajoCC = 0;
  porcPorDebajoCC;

  arrRegistro = [];
  arrValores = [];
  storage: any;

  constructor(private router: Router, 
              private dataService: SendLoteDataService, 
              private _storage: Storage,
              public toastController: ToastController
  ) { }

  ionViewWillEnter() {
    console.log('Cargando nuevos lotes...')
    this.presentToastLoadData();
  }

  async ngOnInit() {
    this.dataService.$getObjectSource.subscribe((data) => {
      this.datosLote = data;
      this.ubicacion = this.datosLote.ubicacion;
      this.campo = this.datosLote.campo;
      this.rodeo = this.datosLote.rodeo;
      this.etapa = this.datosLote.etapa?.descrip;
      this.escala = this.datosLote.escala?.descrip;
      console.log(this.datosLote);
    });
    this.storage = await this._storage.create();
  }

  async goHome() {
    this.registro = 0;
    this.arrRegistro = [];
    this.arrValores = [];
    this.valorTotal = 0;

    let arrayHistoricos = JSON.parse(await this.storage.get('historico'));

    arrayHistoricos = arrayHistoricos.filter((item) => item.campo != this.campo);

    let arrayHistoricosJSON = JSON.stringify(arrayHistoricos);
    this.storage.set('historico', arrayHistoricosJSON);

    this.arrValores = []; 
    this.cantBajoCC = 0;

    // this.dataService.postData(this.datosLote);

    this.router.navigate(['/home']);
  }

  async finMuestreo() {
    let resultado = 0;
    let promedio = 0;
    let promedioUnoCinco = 0;
    
    promedioUnoCinco = +(this.valorTotalUnoCinco / this.arrRegistro.length).toFixed(1);
    promedio = +(this.valorTotal / this.arrRegistro.length).toFixed(1);
    if (this.etapa == 'Parto') {
      resultado = +(this.valorParto(promedio)).toFixed(1);
    } else if (this.etapa == 'Servicio') {
      resultado = +(this.valorServicio(promedio)).toFixed(1);
    } else {
      resultado = +(this.valorDestete(promedio)).toFixed(1);
    }

    
    let values = this.getSD();
    console.log(values);    

    let arrayHistoricos = JSON.parse(await this.storage.get('historico'));
    
    arrayHistoricos.forEach((element, index) => {
      console.log(index);
      
      if (element.campo == this.campo && element.rodeo == this.rodeo && element.ubicacion == this.ubicacion && index == 0) {
        element.registro = this.registro;
        element.resultado = resultado;
        element.valorPromedio = promedio;
        element.desviacionStandar = values.desviacionStandar;
        element.coeficiente = values.coeficiente;
        element.promedioTotalUnoCinco = promedioUnoCinco;
        element.porcentajeBajoCC = this.porcPorDebajoCC;
      }
      return true; 
    });

    let arrayHistoricosJSON = JSON.stringify(arrayHistoricos);
    this.storage.set('historico', arrayHistoricosJSON);

    this.arrValores = [];
    this.cantBajoCC = 0;
    this.registro = 0;

    this.datosLote.valorPromedio = promedio;
    this.datosLote.resultado = resultado;
    (await this.dataService.postData(this.datosLote)).subscribe((data) => {
      console.log('Post al Back: ', data);
    });

    this.router.navigate(['/home']);
  }

  getSD() {
    const variance = this.calculateVariance(this.arrValores);
    const sd = this.calculateSD(variance);
    const prom = this.calculateMean(this.arrValores);
    const coef = (sd/prom) * 100;
    return {desviacionStandar: sd, coeficiente: coef};
  }

  addRegistro(value, n) {
    this.registro++;
    this.arrRegistro.push(this.registro);
    this.arrValores.push(n);

    this.valorTotal += n;

    this.valorTotalUnoCinco += (n / 2 + 0.5);
    
    this.calcularPorcentajeBajoCC(n);

    this.presentToast(value);
  }

  calcularPorcentajeBajoCC(n) { 
    let porcCU = 100 / this.arrValores.length;
    this.cantBajoCC = this.cantBajoCC == 0 ? 0 : this.cantBajoCC;
    console.log(this.etapa, this.escala);
    
    if (this.escala == '1-9') {
      if (this.etapa == 'Parto' && n <= 5) {
        this.cantBajoCC++;
      } else if (this.etapa == 'Servicio' && n <= 5) {
        this.cantBajoCC++;
      } else if (this.etapa == 'Destete' && n <= 6) {
        this.cantBajoCC++;
      }
    } else {
      if (this.etapa == 'Parto' && n <= 3) {
        this.cantBajoCC++;
      } else if (this.etapa == 'Servicio' && n <= 3) {
        this.cantBajoCC++;
      } else if (this.etapa == 'Destete' && n <= 4) {
        this.cantBajoCC++;
      }
    }

    this.porcPorDebajoCC = this.cantBajoCC == 0 ? 0 : porcCU * this.cantBajoCC;
    console.log(this.porcPorDebajoCC);
    
  }

  valorParto(x) {
    if (x < 6 && x > 3) {
      return Math.round(-0.0489 * Math.pow(x, 6) + 1.6778 * Math.pow(x, 5) - 23.191 * Math.pow(x, 4) + 164.3 * Math.pow(x, 3) - 627.55 * Math.pow(x, 2) + 1238.7 * x - 949.92);
    } else if (x <= 7 && x >= 6) {
      return Math.round(0.1391 * Math.pow(x, 4) - 3.9782 * Math.pow(x, 3) + 36.693 * Math.pow(x, 2) - 121.8 * x + 179.83);
    } else if (x <= 3) {
      return 48;
    } else {
      return 95;
    }
  }

  valorServicio(x) {
    if (x < 6 && x > 3) {
      return Math.round(-0.6317 * Math.pow(x, 6) + 19.36 * Math.pow(x, 5) - 242.25 * Math.pow(x, 4) + 1579.7 * Math.pow(x, 3) - 5649.3 * Math.pow(x, 2) + 10505 * x - 7894.2);
    } else if (x <= 7 && x >= 6) {
      return Math.round(1.1342 * Math.pow(x, 4) - 25.032 * Math.pow(x, 3) + 198.21 * Math.pow(x, 2) - 651.53 * x + 806.46);
    } else if (x <= 3) {
      return 50;
    } else {
      return 90;
    }
  }

  valorDestete(x) {
    if (x < 6 && x > 3) {
      return Math.round(-0.0807 * Math.pow(x, 6) + 2.504 * Math.pow(x, 5) - 31.443 * Math.pow(x, 4) + 203.85 * Math.pow(x, 3) - 719.47 * Math.pow(x, 2) + 1321.1 * x - 918.52);
    } else if (x <= 7 && x >= 6) {
      return Math.round(0.4297 * Math.pow(x, 4) - 8.8531 * Math.pow(x, 3) + 64.635 * Math.pow(x, 2) - 191.35 * x + 273.13);
    } else if (x <= 3) {
      return 76;
    } else {
      return 95;
    }
  }

  // Calculate the average of all the numbers
  calculateMean(values) {
    const total = values.reduce((sum, current) => sum + current);
    return total / values.length;
  }

  // Calculate variance
  calculateVariance(values) {
    const average = this.calculateMean(values);
    const squareDiffs = values.map((value) => {
        const diff = value - average;
        return diff * diff;
    });
    const variance = this.calculateMean(squareDiffs);
    return variance;
  }

  // Calculate stand deviation
  calculateSD(variance) {
    return Math.sqrt(variance);
  };

  async presentToast(value) {
    const toast = await this.toastController.create({
      message: 'Se guardo registro de la condicion ' + value,
      duration: 2000
    });
    toast.present();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Toast header',
      message: 'Click to Close',
      icon: 'information-circle',
      position: 'top',
      color: "success",
      buttons: [
        {
          side: 'start',
          icon: 'star',
          text: 'Favorite',
          handler: () => {
            console.log('Favorite clicked');
          }
        }, {
          text: 'Done',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]      
    });
    await toast.present();

    const { role } = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async presentToastLoadData() {
    const toast = await this.toastController.create({
      message: 'Haga click en una imagen para generar registros.',
      duration: 2000
    });
    toast.present();
  }

}
